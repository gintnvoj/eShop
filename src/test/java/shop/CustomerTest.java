package shop;

import cz.cvut.eshop.shop.Customer;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class CustomerTest {
    @Test
    public void constructorPositive() {
        Customer c = new Customer(1);
        assertEquals(1, c.getLoyaltyPoints());
    }

    @Test
    public void constructorZero() {
        Customer c = new Customer(0);
        assertEquals(0, c.getLoyaltyPoints());
    }

    @Test(expected=IllegalArgumentException.class)
    public void constructorNegative() {
        Customer c = new Customer(-1);
    }

    @Test
    public void addLoayltyPointPositive() {
        Customer c = new Customer(1);
        c.addLoayltyPoint(1);
        assertEquals(2, c.getLoyaltyPoints());
    }

    @Test
    public void addLoayltyPointZero() {
        Customer c = new Customer(0);
        c.addLoayltyPoint(0);
        assertEquals(0, c.getLoyaltyPoints());
    }

    @Test(expected=IllegalArgumentException.class)
    public void addLoayltyPointNegative() {
        Customer c = new Customer(0);
        c.addLoayltyPoint(-1);
    }

    @Test
    public void setCustomerAddressNull() {
        Customer c = new Customer(0);
        assertEquals(null, c.getCustomerAddress());
    }

    @Test
    public void setCustomerAddressNonEmpty() {
        Customer c = new Customer(0);
        c.setCustomerAddress("Dvořákovo nábřeží 1427/3");
        assertEquals("Dvořákovo nábřeží 1427/3", c.getCustomerAddress());
    }

    @Test(expected=IllegalArgumentException.class)
    public void setCustomerAddressEmpty() {
        Customer c = new Customer(0);
        c.setCustomerAddress("");
    }

    @Test
    public void setCustomerNameNull() {
        Customer c = new Customer(0);
        assertEquals(null, c.getCustomerName());
    }

    @Test
    public void setCustomerNameNonEmpty() {
        Customer c = new Customer(0);
        c.setCustomerName("Jeremiáš Hanebný");
        assertEquals("Jeremiáš Hanebný", c.getCustomerName());
    }

    @Test(expected=IllegalArgumentException.class)
    public void setCustomerNameEmpty() {
        Customer c = new Customer(0);
        c.setCustomerName("");
    }
}
