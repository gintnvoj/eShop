/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.eshop.shop;

public class Customer {
    private int loyaltyPoints;
    String customerAddress;
    String customerName;

    public Customer(int loyaltyPoints) {
        if (loyaltyPoints < 0) throw new IllegalArgumentException("Negative loyaltyPoints is not allowed!");
        this.loyaltyPoints = loyaltyPoints;
    }

    public int getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void addLoayltyPoint(int loyaltyPoints) {
        if (loyaltyPoints < 0) throw new IllegalArgumentException("Negative loyaltyPoints is not allowed!");
        this.loyaltyPoints += loyaltyPoints;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerAddress(String customerAddress) {
        if (customerAddress == "") throw new IllegalArgumentException("Empty customerAddress is not allowed!");
        this.customerAddress = customerAddress;
    }

    public void setCustomerName(String customerName) {
        if (customerName == "") throw new IllegalArgumentException("Empty customerName is not allowed!");
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }
}
